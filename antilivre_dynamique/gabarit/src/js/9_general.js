// Scripts
const menu = document.querySelector('.btn--menu');
const title = document.querySelector('.title');

menu.addEventListener('click', (e) => {
  e.preventDefault();
  title.classList.toggle('show');
});

const interface = document.querySelector('.interface');
interface.addEventListener('touchend', function(e) {
  document.body.style.overflow = "auto";
});

window.addEventListener("touchstart", function(event) {
  if (event.target.classList.contains('interface') ||
      event.target.classList.contains('level') ||
      event.target.classList.contains('range')) {
    event.preventDefault();
    document.body.style.overflow = "hidden";
  }
}, false);


const textes = document.querySelectorAll('.texte__contenu, .title__info');
textes.forEach(function(e){
  e.innerHTML = e.innerHTML.replace(/(^|<\/?[^>]+>|\s+)([^\s<]+)/g, '$1<span class="mot">$2</span>');

  e.addEventListener('click', function(m) {
    let redacted;
  	if (m.target.classList.contains('mot')) {
  	 	m.target.classList.toggle('redacted');
    }
  });
});


// Custom cursor
const cursor = document.querySelector('.cursor--main');
const cursorBg = document.querySelector('.cursor--bg');
const links = document.querySelectorAll('a, input, .mot');

let cursorX = -100;
let cursorY = -100;
let cursorBgX = -100;
let cursorBgY = -100;
let pageLoad = true;

const cursorMove = () => {
  document.addEventListener('mousemove', (e) => {
    cursorX = e.clientX;
    cursorY = e.clientY;
    if (pageLoad) {
      cursorBgX = e.clientX;
      cursorBgY = e.clientY;
      pageLoad = !pageLoad;
    }
  });

  // Lerp (linear interpolation)
  const lerp = (start, finish, speed) => {
    return (1 - speed) * start + speed * finish;
  };

  const rendering = () => {
    cursorBgX = lerp(cursorBgX, cursorX, 0.1);
    cursorBgY = lerp(cursorBgY, cursorY, 0.1);
    cursor.style.left = `${cursorX}px`;
    cursor.style.top = `${cursorY}px`;
    cursorBg.style.left = `${cursorBgX}px`;
    cursorBg.style.top = `${cursorBgY}px`;
    requestAnimationFrame(rendering);
  };
  requestAnimationFrame(rendering);
}

cursorMove();

// Interaction with the cursor
 links.forEach((e) => {
   e.addEventListener('mouseenter', () => {
     cursor.classList.add('cursor--interact');
   });
   e.addEventListener('mouseleave', () => {
     cursor.classList.remove('cursor--interact');
   });
 });

 document.addEventListener('click', () => {
   cursor.classList.add('cursor--click');

   setTimeout(() => {
     cursor.classList.remove('cursor--click');
   }, 250);
 });


// Dark mode
const btnDark = document.querySelector('.btn--dark');

let darkModeToggle = false;
const darkModeTheme = localStorage.getItem('theme');

if (darkModeTheme) {
  document.documentElement.setAttribute('data-theme', darkModeTheme);

  if (darkModeTheme === 'dark') {
    darkModeToggle = true;
  }
}

function darkModeSwitch() {
  if (!darkModeToggle) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
    darkModeToggle = true;
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
    darkModeToggle = false;
  }
}

btnDark.addEventListener('click', darkModeSwitch);


// Change color
const rangeFg = document.querySelector('.range--fg');

rangeFg.addEventListener("change", () => {
  document.documentElement.style.setProperty('--color-fg', 'rgba(var(--color-alpha-fg),'+ rangeFg.value / 100 + ')');
});

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

