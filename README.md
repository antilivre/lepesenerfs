# ~/ABRÜPT/ANTONIN ARTAUD/LE PÈSE-NERFS/*

La [page de ce livre](https://abrupt.ch/antonin-artaud/le-pese-nerfs/) sur le réseau.

## Sur le livre

Fouiller la moelle ou la langue, jusqu’à mettre les nerfs à vif. Ne jamais rien y expier, mais chercher le néant pour se chercher soi-même. Décomposer les intérieurs de la parole. Et assassiner l’esprit, pour que la vie même se rétracte en un unique point d’une unique durée, et que se déploient seules « la douleur perpétuelle et l’ombre, la nuit de l’âme ».

## Sur l'auteur

Antonin Artaud est une déchirure qui parcourt le réel. L’existence y est aveugle, mais subsistent des encres comme des drogues, qui ont le pouvoir révélateur de s’infiltrer sous les apparences, et c’est de cette puissance augurale qu’Antonin Artaud, de Rodez au pays des Tarahumaras, se revendique sans concession aucune.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
